# Devilbox user-defined settings

Use this folders to add general custom configuration.


CONFIG TO XDEBUG.

[xdebug]
xdebug.mode                    = debug
xdebug.start_with_request      =trigger
xdebug.remote_handler          = dbgp
xdebug.client_port             = 9003
xdebug.idekey                  = PHPSTORM
xdebug.client_host             = host.docker.internal
xdebug.discover_client_host    = 1
#xdebug.remote_log              = /var/log/php/xdebug.log