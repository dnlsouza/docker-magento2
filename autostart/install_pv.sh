#!/usr/bin/env bash

if ! command -v pv >/dev/null 2>&1; then
    if [ ! -d "pv" ]; then
        cd ~
        git clone --depth=1 --single-branch https://github.com/icetee/pv.git
    fi
    cd pv
    ./configure
    make
    make install
    rm -rf pv
fi
