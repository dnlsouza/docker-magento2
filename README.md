<p align="center">
    <h2 align="center">Requisitos mínimos</h1>
</p>

- Docker
- Docker Composer

Pode ser instalado por aqui: <a href="https://docs.docker.com/engine/install/"> Docker </a> e <a href="https://docs.docker.com/compose/install/"> Docker Composer </a>


![Devilbox](docs/img/m2/0.png)


<p align="center">
    <h1 align="center">Instalação do docker local</h1>
</p>

<p>
    1 - Clone o repositorio em uma pasta

![Devilbox](docs/img/m2/1.png)
</p>

<p>
2- Pare todos os serviços que esteja rodando que possa dar conflito, exemplo: apache, mysql, elastic etc.
</p>

<p>
3- acesse a pasta que você clonou

Obs: Os arquivos de env, estão configurados para a versão: 2.4.6 do magento.
Caso queira rodar outra versão do magento, acesse <a href="https://experienceleague.adobe.com/docs/commerce-operations/installation-guide/system-requirements.html"> aqui </a> e veja quais as versões necessárias para: PHP, Elasticsearch, Mysql ou Mariadb.
Após isso, mudar a versão no arquvio: .env

PHP <br>
![Devilbox](docs/img/m2/2.png)

MYSQL OU MARIADB <br>
![Devilbox](docs/img/m2/3.png)

ELASTIC <br>
![Devilbox](docs/img/m2/4.png)

</p>

<p>
4- Após ajustar todo o arquivo .env, rodar o comando: docker-compose up -d dentro da pasta clonada.

![Devilbox](docs/img/m2/5.png)

</p>

<p>
5- Dentro da pasta raiz do docker, acesse: data/www/ aqui, crie uma pasta com o nome da loja, exemplo:
data/www/nomedaloja/

cd data/www/
mkdir nomedaloja
cd nomedaloja

A estrutura precisa ficar assim:

"clone_do_repositorio/data/www/nomedaloja/"

![Devilbox](docs/img/m2/6.png)

</p>

<p>
6- Após isso, configure o seu virtual host.
Linux: cat /etc/hosts
Windows: C:\Windows\System32\drivers\etc\hosts

Adicione uma linha com a seguinte instrução:

127.0.0.1   nomedaloja.loc

Obs: 
    ".loc" obrigatório para todo projeto;
    "nomedaloja" o nome da pasta que você criou na etapa 5.

![Devilbox](docs/img/m2/7.png)

![Devilbox](docs/img/m2/8.png)

</p>

<p>

![Devilbox](docs/img/m2/9.png)

7- Após baixar toda a imagem e rodar o composer, dentro da pasta raiz rode o comando: ./shell.sh e entre na pasta que você criou, exemplo: "nomedaloja".

cd nomedaloja/

![Devilbox](docs/img/m2/10.png)

Ex:
"/nomedaloja/"

Obs: aqui você acessou o "ssh" do docker. Aqui você consegue rodar os comandos via cli, por exemplo: php, composer, mysql etc.
Sempre que precisar rodar comandos do magento, acessar ./shell.sh e a pasta da loja "nomedaloja/htdocs/".
</p>

<p align="center">
    <h2 align="center">Instalação de uma loja existente:</h1>
</p>

<p>
1- Clone o repositorio da loja existente fora do "./shell.sh" dentro da pasta: "clone_do_repositorio/data/www/nomedaloja/" usando o comando:


git clone linkdorepo htdocs/

Ficando com essa estrutura:
"clone_do_repositorio/data/www/nomedaloja/htdocs/arquivos_da_loja"
</p>

<p>
2- Pegue o dump do banco de dados da loja e cole dentro da pasta:

"clone_do_repositorio/backups/mysql/"

adicione o arquivo com o comando via terminal fora do ".shell.sh"

gzip nomedoarquivo.sql nomedoarquivo.sql.gz

3- Siga o passo 3 do processo de Instalação de uma loja nova, para criar um banco de dados.

4- dentro do ".shell.sh" rode o comando para importação do banco de dados: <br>
pv "/shared/backups/mysql/nomedaloja.sql.gz" | gunzip | mysql -h mysql -u root -pmagento "nomedaloja"

Após finalizar o comando, acesse a tabela: "core_config_data" do banco e editar os dados essenciais: <br>
"web/unsecure/base_url" => http://nomedaloja.loc/ <br>
"web/secure/base_url" => https://nomedaloja.loc/

Mude para a url cadastrada no virtual host.

Exemplo: https://nomedaloja.loc/

5- ainda dentro do ".shell.sh" e da pasta do projeto "nomedaloja/htdocs/" rode o comando de instalação "composer install" ou "composer update".

6- Após finalizar o comando, copie o arquivo .env (do servidor da loja existente) e cole o arquivo na pasta: app/etc/

Mude os dados essencias de banco de dados no arquivo:

'db' => [
        'table_prefix' => '',
        'connection' => [
            'default' => [
                'host' => 'mysql',
                'dbname' => 'nomedaloja',
                'username' => 'root',
                'password' => 'magento',
                'active' => '1',
                'driver_options' => [
                    1014 => false
                ],
                'model' => 'mysql4',
                'engine' => 'innodb',
                'initStatements' => 'SET NAMES utf8;'
            ]
        ]
    ],


7- rode o comando "bin/magento setup:upgrade" e "bin/magento setup:di:compile"
</p>

<p align="center">
    <h2 align="center">Instalação de uma loja nova:</h1>
</p>

<p>
IMPORTANTE: Certifique-se de estar na pasta e dentro do ./shell.sh:
"clone_do_repositorio/data/www/nomedaloja/"
</p>

<p>
1- Aqui vamos adicionar as credencias do magento no composer, pegamos essas credencias no site do magento em:
<a href="https://commercemarketplace.adobe.com/customer/accessKeys/"> aqui </a>

composer config --global http-basic.repo.magento.com [Public Key] [Private Key]

![Devilbox](docs/img/m2/11.png)
</p>

<p>
2- Aqui ja estamos prontos para instalar a loja magento padrão, no arquivo commands.txt temos vários comandos para auxiliar o proceso de instalação.

Vamos usar o composer para criar o projeto magento com o comando:
Obs: Atenção com a "VERSION", coloque a versão que você configurou o seu .env para o magento. 

composer create-project --repository-url=https://repo.magento.com/ magento/project-community-edition:VERSION htdocs/


![Devilbox](docs/img/m2/12.png)

![Devilbox](docs/img/m2/12-1.png)
</p>

<p>
3- Após baixar todo o projeto magento, acesse algum aplicativo de manipulação de banco de dados e acesse usando as credenciais:
host: localhost
user: root
password: magento

Crie um banco de dados, exemplo com o comando:
CREATE DATABASE nomedaloja CHARACTER SET utf8 COLLATE utf8_general_ci;

![Devilbox](docs/img/m2/13.png)

![Devilbox](docs/img/m2/13-1.png)

![Devilbox](docs/img/m2/13-2.png)

</p>

<p>
4- Após isso, vamos fazer a instalação da loja magento usando o comando:
IMPORTANTE:
dentro do ".shell.sh" e na pasta "nomedaloja/htdocs/"

![Devilbox](docs/img/m2/14.png)

exemplo:
"clone_do_repositorio/data/www/nomedaloja/htdocs/"

Obs: Mudar os dados dos campos: --db-name, --admin-firstname, --admin-lastname, --admin-email, --admin-user, --admin-password de acordo os dados que você criou.

bin/magento setup:install \
        --db-host=mysql \
        --db-name=nomedaloja \
        --db-user=root \
        --db-password=magento \
        --admin-firstname=Nome \
        --admin-lastname=Sobrenome \
        --admin-email=email@gmail.com.br \
        --admin-user=user \
        --admin-password=pass123 \
        --backend-frontname=admin \
    --cleanup-database


![Devilbox](docs/img/m2/15.png)
![Devilbox](docs/img/m2/15-1.png)
</p>

<p>
5 - Configure as urls da loja com o nome que você criou o projeto:
bin/magento config:set web/unsecure/base_url 'http://nomedaloja.loc/' ; bin/magento config:set web/secure/base_url 'https://nomedaloja.loc/'

![Devilbox](docs/img/m2/16.png)
</p>

<p>
6 - Após isso, rode o comando: bin/magento setup:upgrade ; bin/magento setup:di:compile

Acesse a loja e veja se foi instalado corretamente.


![Devilbox](docs/img/m2/17.png)
![Devilbox](docs/img/m2/17-1.png)
</p>