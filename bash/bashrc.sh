# Bashrc folder
#
# 1. This folder will be mounted to /etc/bashrc-devilbox.d
# 2. All files ending by *.sh will be sourced by bash automatically
#    for the devilbox and root user.
#


# Add your custom vimrc and always load it with vim.
# Also make sure you add vimrc to this folder.
alias vim='vim -u /etc/bashrc-devilbox.d/vimrc'

alias m2='bin/magento'
alias m2f='bin/magento setup:upgrade && bin/magento setup:di:compile'
alias m2u='bin/magento setup:upgrade'
alias m2c='bin/magento setup:di:compile'
